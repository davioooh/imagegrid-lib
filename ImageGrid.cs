﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace Davioooh.ImageGrid
{
    public class ImageGrid
    {
        public int Rows { private set; get; }
        public int Cols { private set; get; }
        public int CellSize { private set; get; }

        public IList<IList<GridElement>> _grid;

        #region Contructors

        private ImageGrid() { }

        #endregion

        #region Public methods

        /// <summary>
        /// Initialize ImageGrid structure parsing XML config file.
        /// </summary>
        /// <param name="uri">Config file URI.</param>
        /// <returns>A new instance of ImageGrid object.</returns>
        public static ImageGrid FromFile(string uri)
        {
            ImageGrid grid = new ImageGrid();
            grid.SetGrid(XDocument.Load(uri));
            return grid;
        }

        /// <summary>
        /// Initialize ImageGrid structure parsing an XML string.
        /// </summary>
        /// <param name="cfg">XML string.</param>
        /// <returns>A new instance of ImageGrid object.</returns>
        public static ImageGrid New(string cfg)
        {
            ImageGrid grid = new ImageGrid();
            grid.SetGrid(XDocument.Parse(cfg));
            return grid;
        }

        /// <summary>
        /// Initialize ImageGrid structure with the specified size.
        /// </summary>
        /// <param name="rows">Number of rows of the grid.</param>
        /// <param name="cols">Number of columns of the grid.</param>
        /// <param name="cellSize">Size of the side for the basic square cell</param>
        /// <returns>A new instance of ImageGrid object.</returns>
        public static ImageGrid New(int rows, int cols, int cellSize)
        {
            ImageGrid grid = new ImageGrid();

            grid.Rows = rows;
            grid.Cols = cols;
            grid.CellSize = cellSize;

            grid._grid = new List<IList<GridElement>>(grid.Rows);
            for (int r = 0; r < grid.Rows; r++)
            {
                grid._grid.Add(new List<GridElement>(cols));
                for (int c = 0; c < grid.Cols; c++)
                {
                    grid._grid[r].Add(new GridElement()
                    {
                        Width = grid.CellSize,
                        Height = grid.CellSize,
                        PositionX = grid.CellSize * c,
                        PositionY = grid.CellSize * r,
                    });
                }
            }

            return grid;
        }

        /// <summary>
        /// Generate the mosaic picture from the provided image uris.
        /// </summary>
        /// <param name="imageUris">The array of uris of the images that will be used in the mosaic.</param>
        /// <param name="crop">Optional parameter to crop images to square size. If false (default value) the images will be stretched to fit square cell.</param>
        /// <param name="opacity">Optional parameter to set image opacity (values from 0 to 1)</param>
        /// <param name="backgroundColorHex">Optional parameter to set background color expressed in HTML format (ex. "#000000" for black)</param>
        /// <returns>The Image representing the mosaic.</returns>
        public Image GetImageGrid(string[] imageUris, bool crop = false, float opacity = 1, string backgroundColorHex = "")
        {
            Image[] imgs = imageUris.Select(uri => Bitmap.FromFile(uri)).ToArray();
            return GetImageGrid(imgs, crop, opacity, backgroundColorHex);
        }

        /// <summary>
        /// Generate the mosaic picture from the provided Image objects.
        /// </summary>
        /// <param name="images">The array of images that will be used in the mosaic.</param>
        /// <param name="crop">Optional parameter to crop images to square size. If false (default value) the images will be stretched to fit square cell.</param>
        /// <param name="opacity">Optional parameter to set image opacity (values from 0 to 1)</param>
        /// <param name="backgroundColorHex">Optional parameter to set background color expressed in HTML format (ex. "#000000" for black)</param>
        /// <returns>The Image representing the mosaic.</returns>
        public Image GetImageGrid(Image[] images, bool crop = false, float opacity = 1, string backgroundColorHex = "")
        {
            Image outImage = new Bitmap(CellSize * Cols, CellSize * Rows);

            using (Graphics graph = Graphics.FromImage(outImage))
            {
                // set the background color (default transparent)
                if (!string.IsNullOrEmpty(backgroundColorHex))
                {
                    graph.Clear(System.Drawing.ColorTranslator.FromHtml(backgroundColorHex));
                }

                // set the color (opacity) of the image  
                ImageAttributes attributes = new ImageAttributes();
                attributes.SetColorMatrix(
                    new ColorMatrix() { Matrix33 = opacity }
                    , ColorMatrixFlag.Default
                    , ColorAdjustType.Bitmap);

                int imgCount = 0;
                for (int r = 0; r < Rows; r++)
                {
                    for (int c = 0; c < Cols; c++)
                    {
                        if (!_grid[r][c].IsEmpty)
                        {
                            Bitmap cellImage = new Bitmap(images[imgCount]);
                            if (crop)
                            {
                                cellImage = GetSquareImage(cellImage);
                            }     

                            // draw the image  
                            graph.DrawImage(
                                cellImage
                                , new Rectangle(
                                    _grid[r][c].PositionX
                                    , _grid[r][c].PositionY
                                    , _grid[r][c].Width
                                    , _grid[r][c].Height)
                                , 0
                                , 0
                                , cellImage.Width
                                , cellImage.Height
                                , GraphicsUnit.Pixel
                                , attributes);
                            imgCount = imgCount == images.Count() - 1 ? 0 : imgCount + 1;
                        }
                    }
                }
            }
            return outImage;
        }

        #endregion

        #region Private methods

        private void SetGrid(XDocument xml)
        {
            int rowsCount = 0;
            XAttribute rowsAttr = xml.Element("grid").Attribute("rows");
            if (rowsAttr == null || !int.TryParse(rowsAttr.Value, out rowsCount))
            {
                throw new ArgumentException("rows not specified");
            }
            Rows = rowsCount;
            //
            int colsCount = 0;
            XAttribute colsAttr = xml.Element("grid").Attribute("cols");
            if (colsAttr == null || !int.TryParse(colsAttr.Value, out colsCount))
            {
                throw new ArgumentException("cols not specified");
            }
            Cols = colsCount;
            //
            int l = 0;
            XAttribute cellsizeAttr = xml.Element("grid").Attribute("cellsize");
            if (cellsizeAttr == null || !int.TryParse(cellsizeAttr.Value, out l))
            {
                throw new ArgumentException("cellsize not specified");
            }
            CellSize = l;

            _grid = new List<IList<GridElement>>(Rows);
            var tableRows = xml.Element("grid").Elements("tr").ToList();
            for (int r = 0; r < rowsCount; r++)
            {
                _grid.Add(new List<GridElement>(Cols));
                var tableCells = tableRows[r].Elements("td").ToList();
                for (int c = 0; c < colsCount; c++)
                {
                    var cell = tableCells[c];
                    GridElement el = new GridElement()
                    {
                        Width = l,
                        Height = l,
                        PositionX = l * c,
                        PositionY = l * r,
                    };

                    XAttribute colspan = cell.Attribute("colspan");
                    if (colspan != null)
                    {
                        el.Width = l * int.Parse(colspan.Value);
                    }
                    XAttribute rowspan = cell.Attribute("rowspan");
                    if (rowspan != null)
                    {
                        el.Height = l * int.Parse(rowspan.Value);
                    }
                    XAttribute emptyAttr = cell.Attribute("empty");
                    el.IsEmpty = emptyAttr != null;

                    _grid[r].Add(el);
                }
            }
        }

        private Bitmap GetSquareImage(Bitmap bitmap)
        {
            Bitmap cropped = bitmap;
            if (bitmap.Width != bitmap.Height)
            {
                int x = 0;
                int y = 0;
                int l = 0;

                if (bitmap.Width > bitmap.Height)
                {
                    l = bitmap.Height;
                    x = (bitmap.Width - bitmap.Height) / 2;
                }
                else
                    if (bitmap.Width < bitmap.Height)
                    {
                        l = bitmap.Width;
                        y = (bitmap.Height - bitmap.Width) / 2;
                    }

                Rectangle rect = new Rectangle(x, y, l, l);
                cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            }
            return cropped;
        }

        #endregion

        #region Nested classes

        public class GridElement
        {
            public int PositionX { get; set; }
            public int PositionY { get; set; }
            public int Height { get; set; }
            public int Width { get; set; }
            public bool IsEmpty { get; set; }
        }

        #endregion
    }
}
