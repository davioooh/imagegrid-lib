# ImageGrid lib #

.NET utility class to generate image mosaic with a configurable grid structure.

### Contribution ###

* If you want to contribute to this project, please contact me.

### Notes ###

* Current stable version is 1.0.0

### For suggestions and info ###

* e-mail: **castellettid (at) gmail [dot] com**

### Support project ###

* If you find this project useful, please make a donation: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=265EAR835B5WG


### Usage ###

* Download the jar file in download section and import it into your .NET web project.

#### Configuration Sintax ####

The sintax for grid configuration is very simple and inspired to HTML TABLE structure.

You define a ```<grid>``` element. The dimention for the mosaic grid is specified using ```rows``` and ```cols``` attributes. The ```cellsize``` attribute is the size in pixel for the basic cell-unit of the grid.

A grid contains several rows. Rows are defined using ```<tr>``` tag.

Cells in a row are defined using ```<td>``` tag. A mosaic element can span among several cells and rows, so you can specify a ```rowspan``` and ```colspan``` greater than 1 for your cells.
The following is a sample configuration XML:

```
#!xml

<?xml version="1.0" encoding="utf-8" ?>
<grid rows='5' cols='4' cellsize='200'>
  <tr>
    <td />
    <td/>
    <td/>
    <td/>
  </tr>
  <tr>
    <td rowspan='2' colspan='2' />
    <td empty='empty'/>
    <td/>
    <td/>
  </tr>
  <tr>
    <td empty='empty' />
    <td empty='empty'/>
    <td rowspan='2' colspan='2' />
    <td empty='empty'/>
  </tr>
  <tr>
    <td/>
    <td/>
    <td empty='empty' />
    <td empty='empty'/>
  </tr>
  <tr>
    <td/>
    <td/>
    <td/>
    <td/>
  </tr>
</grid>

```

#### Sample Image ####

This is a sample mosaic generated using the previous configuration:

![download_.png](https://bitbucket.org/repo/bXBA7b/images/750033893-download_.png)